//bai1
/* đầu vào: số tiền lương 1 ngày
    các bước sử lý:
    b1: tạo 1 biến chứa só tiền lương 1 ngày edge1
    b2:tạo 1 biến nhập ngày làm edge2
    b3: tạo 1 biến tính tiền lương result
    b4:gán giá trị cho edgj1,edgj2
    b5: tiền lương= lương một ngày *số ngày làm
    b6:in ra màn hình
    đầu ra:số tiền lương nhân viên
 */
    var edge1=100000;
    var edge2=24;
    var result;
    result=edge1*edge2;
    console.log('tienluong',result);
    /*
    bai2
    đầu vào: 5 số thực
    b1: tạo 5 biến chứa 5 số thực
    b2:tạo biến tính giá trị trung bình
    b3:gán giá trị cho 5 biến
    b4:giá trị trung bình=(edge1+edge2+edge3+edge4+edge5)/5
    b5:in ra màn hình
    đầu ra: giá trị trung bình
     */
    var edge1=2;
    var edge2=5;
    var edge3=6;
    var edge4=7;
    var edge5=8;
    var result;
    result =(edge1+edge2+edge3+edge4+edge5)/5;
    console.log('gia tri trung binh',result);
    /*
    bai3
    đầu vào: cho người dùng nhập số tiền usd
    b1: tạo biến nhập số tiền
    b2: tạo biến chứa giá trị tiền hiện nay
    b3: gán giá trị cho 2 biến
    b4:tính tiền= edge1*edge2
    b5:in ra màn hình
     */
    var edge1=4;
    var edge2=23500;
    var result;
    result=edge1*edge2;
    console.log('số tiền quy đổi sang VND:',result);
    /*
    bai4
    đầu vào: nhập chiều dài chiều rộng 
    b1:tạo 2 biến chứa chiều dài edge1 và chiều rộng edge1
    b2: tạo 2 biến tính chu vi edge4 và diện tích edge5
    b3: gán giá trị cho chiều dài và rộng
    b4: diện tích=dài*rộng,chu vi=(dài+rộng)*2
    b5 in ra màn hình
     */
    var edge1=10;
    var edge2=20;
    var edge4;
    var edge5;
     edge4=(edge1+edge2)*2;
     edge5=edge1*edge2;
     console.log("hinh chu nhat co chu vi la",edge4);
     console.log("hinh chu nhat co dien tich la",edge5);
    /*
    bai5
    đầu vào: nhập vào 1 số có hai chữ số
    b1:  tạo một biến số có hai chữ số n,biến hàng đơn vị và hàng chục
    b2: gán giá trị cho n
    b3:tách hàng đơn vị theo công thức: so%10
    b4:tách hàng chục theo công thức:so/10
    b5: in kết quả
    đầu ra: kết quả sum của 2 chữ số đơn vị và hàng chục
     */
    var number=59;
    var donVi=number%10;
    var hangChuc=Math.floor(number/10)%10;
    var result;
    result=donVi+hangChuc
    console.log('tong',result);